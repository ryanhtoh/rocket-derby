extends Panel

var user
var time = 0.0

func _on_close_pressed():
	user.busy = false
	hide()

func activate(by_whom):
	user = by_whom
	show()
