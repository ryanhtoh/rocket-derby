extends Node2D

var goal_vec
var vel_vec
var vec_color = Color.red

func _process(delta):
	position = get_viewport().size/2
	update()

func _draw():
	draw_line(Vector2.ZERO, vel_vec, vec_color, 4)
	draw_line(Vector2.ZERO, goal_vec, Color.darkgreen, 1)
