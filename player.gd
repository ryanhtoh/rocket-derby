extends RigidBody2D

const THRUST = Vector2(0, -100.0)
const RCS_THRUST = 400
const GIMBAL_ANG = 0.12
const KARMAN_LINE = 20000
const MAX_DAMP = 0.12
const CRASH_SPEED = 60
const MAX_FUEL = 100

var gimbal = 0.0
var fuel = 10
var mass_initial = 1.5
var refueling = false
sync var thrusting = false
sync var to_reset = false
var goal = 1
var p_id

onready var ui = get_tree().get_root().get_node("world/UI")
onready var loc = ui.get_node("loc")
onready var vel_Vec = ui.get_node("vel_Vec")
onready var barges = get_tree().get_root().get_node("world/barges")
onready var next_goal_node = barges.get_node(str(goal))

var game_done = false
var alive = true

puppet var flame_vis
puppet var flame_rot
puppet var pos
puppet var rot
puppet var vel
puppet var ang_vel

var buf2_v
var buf_v

func _ready():
	if is_network_master():
		ui.player = self

func set_player_name(new_name):
	get_node("label").set_text(new_name)

func _process(delta):
	$label.rect_rotation = -rotation
	update()

func _integrate_forces(state):
	buf2_v = buf_v
	buf_v = linear_velocity
	if to_reset:
		$rocket.show()
		state.set_transform( Transform2D(0, Vector2(0,-80)))
		state.linear_velocity = Vector2(0, 0)
		state.angular_velocity = 0
		fuel = 10
		alive = true
		to_reset = false
		
	if not is_network_master():
		state.set_transform( Transform2D(rot, pos))
		state.linear_velocity = vel
		state.angular_velocity = ang_vel
		

func _physics_process(delta):
	if is_network_master():
		if fuel <= 0:
			ui.get_node("Fuel_Reset").visible = true
		else:
			ui.get_node("Fuel_Reset").visible = false
		
		loc.text = str("Elapsed Secs: ", str(stepify(gamestate.time_elapsed, 0.1)),
		"\nPosition: (", stepify(position.x, 1), ", ", stepify(-position.y, 1), 
		")\nVelocity: (", stepify(linear_velocity.x, 1), ", ", stepify(-linear_velocity.y, 1),
		")\nTarget Distance: ", stepify((next_goal_node.position - position).length(), 1),
		"\nAir Density: ", stepify(linear_damp*10, 0.01), " kg/m3")
		
		vel_Vec.vel_vec = linear_velocity / $camera.get_zoom()
		if linear_velocity.length() < CRASH_SPEED:
			vel_Vec.vec_color = Color.darkgreen
		else:
			vel_Vec.vec_color = Color.red
		vel_Vec.goal_vec = (next_goal_node.position - position) / $camera.get_zoom()
		if refueling and fuel <= MAX_FUEL:
			fuel += delta * 10
		ui.get_node("refueling/bar").value = fuel
		ui.get_node("refueling").text = str("%.1f" % fuel, "% / sec")
		linear_damp = max(0, MAX_DAMP*(KARMAN_LINE + position.y)/KARMAN_LINE)
		var rcs = 0
		
		if Input.is_action_pressed("thrust") and fuel > 0:
			rset_unreliable("thrusting", true)
		else:
			rset_unreliable("thrusting", false)
		
		if Input.is_action_pressed("move_left") and alive and fuel > 0:
			rpc_unreliable("rcs", 1)
			rcs = -RCS_THRUST
			gimbal = GIMBAL_ANG
		elif Input.is_action_pressed("move_right") and alive and fuel > 0:
			rpc_unreliable("rcs", -1)
			rcs = RCS_THRUST
			gimbal = -GIMBAL_ANG
		else:
			rpc_unreliable("rcs", 0)
			gimbal = 0
		
		if thrusting:
			var thrust = THRUST.rotated(gimbal).rotated(rotation)
			apply_impulse(Vector2(0, 45.8).rotated(rotation), thrust*delta)
			fuel -= delta
			mass = mass_initial * (30 + fuel)/130
		if fuel > 0:
			apply_torque_impulse(rcs*delta)
			fuel -= delta*0.2*abs(rcs)/RCS_THRUST
		rpc_unreliable("sync", thrusting, gimbal, position, rotation, linear_velocity, angular_velocity)
	
	# For all instances, master and slave
	if thrusting and alive:
		$RCS/thrust_inner.emitting = true
		$RCS/thrust_outer.emitting = true
		$RCS/thrust_inner.rotation = gimbal
		$RCS/thrust_outer.rotation = gimbal
		$audio/thrust.stream_paused = false
	else:
		$RCS/thrust_inner.emitting = false
		$RCS/thrust_outer.emitting = false
		$audio/thrust.stream_paused = true

# RCS particles
sync func rcs(dir):
	if dir == -1:
		$RCS/topright.emitting = false
		$RCS/topleft.emitting = true
		$RCS/botright.emitting = true
		$RCS/botleft.emitting = false
	elif dir == 1:
		$RCS/topright.emitting = true
		$RCS/topleft.emitting = false
		$RCS/botright.emitting = false
		$RCS/botleft.emitting = true
	else:
		$RCS/topright.emitting = false
		$RCS/topleft.emitting = false
		$RCS/botright.emitting = false
		$RCS/botleft.emitting = false

sync func sync(a, b, c, d, e, f):
	thrusting = a
	gimbal = b
	pos = c
	rot = d
	vel = e
	ang_vel = f

sync func explode(body):
	alive = false
	$boom.emitting = true
	$boom/Timer.start(1.0)
	if body.name == "water":
		$audio/splash.play()
	else:
		$audio/boom.play()
	$rocket.hide()

func _on_player_body_entered(body):
	if is_network_master() and alive:
		if buf2_v.length() > CRASH_SPEED or body.name == "water":
			rpc("explode", body)
		elif body.is_in_group("barges"):
			refueling = true
			if body.name == str(goal):
				ui.get_node("ding").play()
				goal += 1
				ui.get_node("Score").rpc("increase_score", p_id)
				if  ui.get_node("Score").player_labels[p_id].score > 4 and not game_done:
					rpc("show_winner", get_node("label").get_text())
				else:
					next_goal_node = barges.get_node(str(goal))

sync func reset(pos_in):
	to_reset = true

func _on_player_body_exited(body):
	if is_network_master():
		refueling = false

sync func show_winner(winner):
	ui.get_node("Winner").text = str("Winner is: ", winner )
	ui.get_node("Winner").show()
	game_done = true

func _on_Timer_timeout():
	if is_network_master():
		rpc("reset", Vector2(0,-80))
