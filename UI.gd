extends CanvasLayer

var player

func _on_Timer2_timeout():
	player.rpc("reset", Vector2(0,-80))
	$Fuel_Reset.visible = false


func _on_Help_pressed():
	$Help_Popup.visible = true


func _on_Button_pressed():
	$Help_Popup.visible = false


func _on_Reset_pressed():
	player.rpc("reset", Vector2(0,-80))
