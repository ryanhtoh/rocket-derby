extends KinematicBody2D

export var label_name = "Barge"

func _ready():
	$Label.text = label_name
	add_to_group("barges")
